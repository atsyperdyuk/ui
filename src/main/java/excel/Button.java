package excel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andriy on 9/5/2017.
 */
public class Button {

    public List<ExpenseType> expenseTypes = new ArrayList<ExpenseType>();
    public String name = "";
    public String icon = "";
    public int position = 0;

    public int getPosition() {
        return position;
    }

    public void addExpType(ExpenseType expenseType){
        this.expenseTypes.add(expenseType);
    }

    public List<ExpenseType> getExpenseTypes() {
        return expenseTypes;
    }


    public String getName() {
        return name;
    }

    public String getIcon() {
        return icon;
    }

    Button(String name, String icon, int position)
    {
        this.name = name;
        this.icon = icon;
        this.position = position;
    }

    @Override
    public String toString() {
        String s = name + " | " + icon + " row: " + position;
        return s;
    }
}
