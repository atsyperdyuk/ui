package excel;

/**
 * Created by Andriy on 9/5/2017.
 */
public class ExpenseType {
    public String name = "";
    public String icon = "";
    public int number = 0;

    ExpenseType(String name, String icon, int number){
        this.name = name;
        this.icon = icon;
        this.number = number;
    }


    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        String s = name + " | " + icon;
        return s;
    }
}
