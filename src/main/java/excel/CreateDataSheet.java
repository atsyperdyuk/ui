package excel;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

import static excel.CreatingExpBars.expenseBarsList;
import static excel.InitWorkBook.*;

/**
 * Created by Andriy on 9/18/2017.
 */
public class CreateDataSheet {

    public static void add(){
        int i = 0;
        myExcelSheet = myExcelBook.createSheet("Data");
        row = myExcelSheet.createRow(0);
        style = myExcelSheet.getWorkbook().createCellStyle();
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.YELLOW.index);

        //creating first sheet
        row.createCell(0).setCellValue("DEFINE");
        row.createCell(1).setCellValue("Integer");
        row.createCell(2).setCellValue("~Customer");
        row.createCell(3).setCellStyle(style);

        row = myExcelSheet.createRow(1);
        row.createCell(0).setCellValue("DEFINE");
        row.createCell(1).setCellValue("Integer");
        row.createCell(2).setCellValue("~CostCode");
        row.createCell(3).setCellStyle(style);

        for (ExpenseBar expBar : expenseBarsList) {
            row = myExcelSheet.createRow(2 + i);
            row.createCell(0).setCellValue("DEFINE");
            row.createCell(1).setCellValue("Integer");
            row.createCell(2).setCellValue("~ExpBar" + expBar.getMosaicName());
            row.createCell(3).setCellStyle(style);
            i++;
        }

        // changing size of columns
        myExcelSheet.autoSizeColumn(0);
        myExcelSheet.autoSizeColumn(1);
        myExcelSheet.autoSizeColumn(2);
        myExcelSheet.autoSizeColumn(3);
    }
}
