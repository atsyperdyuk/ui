package excel;

import javax.swing.*;

import static excel.InitWorkBook.myExcelBook;

/**
 * Created by Andriy on 8/31/2017.
 */
public class MessageBox extends JFrame {

    public static void display(String message){
        try {
            JOptionPane.showMessageDialog(new MessageBox(), message);
            myExcelBook.close();
        }catch (Exception e){

        } finally {
            System.exit(0);
        }
    }
}
