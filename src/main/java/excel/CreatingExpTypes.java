package excel;

import static excel.CreatingExpBars.expenseBarsList;
import static excel.InitWorkBook.myExcelSheet;
import static excel.InitWorkBook.row;
import static excel.ReadFromExcel.position;

/**
 * Created by Andriy on 9/11/2017.
 */
public class CreatingExpTypes {

    static final int EXPENSE_NAME = 4;
    static final int EXPENSE_ICON = 5;
    static String expenseName = "";
    static String expenseIcon = "";

    public static void getExpTypes(){
        for (int i = 0; i < expenseBarsList.size(); i++) {
            position = expenseBarsList.get(i).getStartPosition();
            for (int j = 0; j < expenseBarsList.get(i).getButtons().size(); j++) {
                position = expenseBarsList.get(i).getButtons().get(j).getPosition();
                row = myExcelSheet.getRow(position);
                while (row != null && row.getCell(EXPENSE_NAME) != null && row.getCell(EXPENSE_NAME).toString().length() > 0) {
                   expenseName = row.getCell(EXPENSE_NAME).toString();
                   if (expenseName.length() > 0) {
                       //if expense icon is missed
                       if (row.getCell(EXPENSE_ICON) == null || row.getCell(EXPENSE_ICON).toString().length() == 0) {
                           MessageBox.display("The expense icon is missed, see row " + ++position);
                       }
                       expenseIcon = row.getCell(EXPENSE_ICON).toString();
                       Data.countExpTypes++;
                       expenseBarsList.get(i).getButtons().get(j).addExpType(new ExpenseType(expenseName, expenseIcon, Data.countExpTypes));

                   }
                   position++;
                   row = myExcelSheet.getRow(position);
                }
            }
        }
    }

    public static void display(){
        for (ExpenseBar expBar : expenseBarsList){
            System.out.println(expBar.getMosaicName());
            for (Button button : expBar.getButtons()){
                System.out.println("    " + button.getName());
                for(ExpenseType expType : button.getExpenseTypes()){
                    System.out.println("          " + expType.getName());
                }
            }
        }
    }

}
