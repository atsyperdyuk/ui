package excel;

import static excel.InitWorkBook.myExcelBook;
import static excel.InitWorkBook.myExcelSheet;
import static excel.InitWorkBook.row;


/**
 * Created by Andriy on 8/27/2017.
 */
public class ReadFromExcel {
    static int position = 0;

    public static void readFromExcel(String file) throws Exception{

        //declaration variables
        final int MOSAIC = 0;

        //initialization workbook
        InitWorkBook.getWorkBook(file);

        //finding start position
        for (int i = 0; i < 100; i++) {
            row = myExcelSheet.getRow(i);
            if (row != null && row.getCell(MOSAIC) != null && row.getCell(MOSAIC).toString().matches("Mosaic")) {
                position = i;
                break;
            }
        }

        //if there is no any expense bar in the Workbook
        if (position == 0) {
            MessageBox.display("Can't find any 'Mosaic' in the workbook");
        }

        //creating array of expense bars
        CreatingExpBars.getExpenseBars();
        //CreatingExpBars.display();

        //creating array of buttons
        CreatingButtons.getButtons();
        //CreatingButtons.display();

        //creating array of expense types
        CreatingExpTypes.getExpTypes();
        //CreatingExpTypes.display();

        myExcelBook.close();
    }
}
