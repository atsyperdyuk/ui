package excel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import static excel.InitWorkBook.myExcelBook;
import java.io.FileOutputStream;


/**
 * Created by Andriy on 8/27/2017.
 */
public class WriteIntoExcel {

    public static void writeIntoExcel(String file) throws Exception {


        myExcelBook = new HSSFWorkbook();

        CreateDataSheet.add();
        CreateExpenseTypesSheet.add();
        CreateButtonsSheet.add();
        CreateButtonMenuItemsSheet.add();

        // write data into the file
        myExcelBook.write(new FileOutputStream(file));
        myExcelBook.close();
    }
}
