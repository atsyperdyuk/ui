package excel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andriy on 9/5/2017.
 */
public class ExpenseBar {
    public List<Button> button = new ArrayList<Button>();
    public int startPosition = 0;
    public String mosaicName = "";
    public String mosaicType = "";

    ExpenseBar(String mosaicName, String mosaicType, int startPosition) {
        this.mosaicName = mosaicName;
        this.mosaicType = mosaicType;
        this.startPosition = startPosition;
    }

    public List<Button> getButtons() {
        return button;
    }

    public int getButtonSize() {
        return button.toArray().length;
    }

    public void addButton(Button button){
        this.button.add(button);
    }

    public int getStartPosition() {
        return startPosition;
    }

    public String getMosaicName() {
        return mosaicName;
    }

    @Override
    public String toString() {
        String s = mosaicName + " | " + mosaicType + " -> " + new Integer(startPosition).toString();
        return s;
    }
}
