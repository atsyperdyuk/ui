package excel;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

import static excel.CreatingExpBars.expenseBarsList;
import static excel.InitWorkBook.font;
import static excel.InitWorkBook.*;

/**
 * Created by Andriy on 9/18/2017.
 */
public class CreateButtonsSheet {

    public static void add(){
        int position = 0;
        myExcelSheet = myExcelBook.createSheet("Buttons");
        row = myExcelSheet.createRow(position);
        style = myExcelSheet.getWorkbook().createCellStyle();
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        font = myExcelBook.createFont();
        font.setBold(true);
        style.setFont(font);

        //updating the table tbl_Button
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_Button");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ButtonID");
        row.createCell(2).setCellValue("ExpenseBarID");
        row.createCell(3).setCellValue("CustomerID");
        row.createCell(4).setCellValue("Icon");
        row.createCell(5).setCellValue("Label");
        row.createCell(6).setCellValue("OffsetY");
        row.createCell(7).setCellValue("SortOrder");
        row.createCell(8).setCellValue("ExpenseReportItemTypeID");
        row.createCell(9).setCellValue("FlexID");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);
        row.getCell(4).setCellStyle(style);
        row.getCell(5).setCellStyle(style);
        row.getCell(6).setCellStyle(style);
        row.getCell(7).setCellStyle(style);
        row.getCell(8).setCellStyle(style);
        row.getCell(9).setCellStyle(style);
        int expBarCounter = 0;
        int i = 1;
        position++;
        for (ExpenseBar expBar : expenseBarsList){
            int sortOrder = 10;
            for (Button button : expBar.getButtons()){
                row = myExcelSheet.createRow(position);
                row.createCell(0).setCellValue("insert");
                row.createCell(1).setCellValue("~b" + i);
                row.createCell(2).setCellValue("~ExpBar" + expenseBarsList.get(expBarCounter).getMosaicName());
                row.createCell(3).setCellValue("~Customer");
                row.createCell(4).setCellValue(button.getIcon());
                row.createCell(5).setCellValue(button.getName());
                row.createCell(6).setCellValue("0");
                row.createCell(7).setCellValue(sortOrder);
                if(button.getExpenseTypes().size() == 1){
                    row.createCell(8).setCellValue("~e" + button.getExpenseTypes().get(0).getNumber());
                }
                i++;
                position++;
                sortOrder += 10;
            }
            expBarCounter++;
        }
        position += 2;

        //updating the table tbl_ScreenObject
        row = myExcelSheet.createRow(position);
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_ScreenObject");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ScreenObjectID");
        row.createCell(2).setCellValue("CustomerID");
        row.createCell(3).setCellValue("FlexID");
        row.createCell(4).setCellValue("MobileFlag");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);
        row.getCell(4).setCellStyle(style);
        position++;
        i = 1;
        for (ExpenseBar expBar : expenseBarsList){
            for (Button button : expBar.getButtons()){
                row = myExcelSheet.createRow(position++);
                row.createCell(0).setCellValue("insert");
                row.createCell(1).setCellValue("~bso" + i);
                row.createCell(2).setCellValue("~Customer");
                row.createCell(3).setCellValue("~concat('lbl_tbl_Button_',~b" + i +")");
                row.createCell(4).setCellValue("0");
                i++;

            }
        }
        position += 2;

        //updating the table tbl_ScreenObjectDescription
        row = myExcelSheet.createRow(position);
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_ScreenObjectDescription");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ScreenObjectID");
        row.createCell(2).setCellValue("LanguageID");
        row.createCell(3).setCellValue("TheValue");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);
        i = 1;
        for (ExpenseBar expBar : expenseBarsList){
            for (Button button : expBar.getButtons()){
                row = myExcelSheet.createRow(++position);
                row.createCell(0).setCellValue("insert");
                row.createCell(1).setCellValue("~bso" + i);
                row.createCell(2).setCellValue("1");
                row.createCell(3).setCellValue(button.getName());
                i++;
            }
        }
        position += 2;

        //updating the table tbl_Button
        row = myExcelSheet.createRow(++position);
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_Button");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ButtonID");
        row.createCell(2).setCellValue("ExpenseBarID");
        row.createCell(3).setCellValue("CustomerID");
        row.createCell(4).setCellValue("Icon");
        row.createCell(5).setCellValue("Label");
        row.createCell(6).setCellValue("OffsetY");
        row.createCell(7).setCellValue("SortOrder");
        row.createCell(8).setCellValue("ExpenseReportItemTypeID");
        row.createCell(9).setCellValue("FlexID");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);
        row.getCell(4).setCellStyle(style);
        row.getCell(5).setCellStyle(style);
        row.getCell(6).setCellStyle(style);
        row.getCell(7).setCellStyle(style);
        row.getCell(8).setCellStyle(style);
        row.getCell(9).setCellStyle(style);
        expBarCounter = 0;
        i = 1;
        for (ExpenseBar expBar : expenseBarsList){
            int sortOrder = 10;
            for (Button button : expBar.getButtons()){
                row = myExcelSheet.createRow(++position);
                row.createCell(0).setCellValue("update");
                row.createCell(0).setCellValue("update");
                row.createCell(1).setCellValue("~b" + i);
                row.createCell(2).setCellValue("~ExpBar" + expenseBarsList.get(expBarCounter).getMosaicName());
                row.createCell(3).setCellValue("~Customer");
                row.createCell(4).setCellValue(button.getIcon());
                row.createCell(5).setCellValue(button.getName());
                row.createCell(6).setCellValue("0");
                row.createCell(7).setCellValue(sortOrder);
                if(button.getExpenseTypes().size() == 1){
                    row.createCell(8).setCellValue("~e" + button.getExpenseTypes().get(0).getNumber());
                }
                row.createCell(9).setCellValue("~concat('lbl_tbl_Button_',~b" + i +")");
                i++;
                sortOrder += 10;
            }
            expBarCounter++;
        }

        myExcelSheet.autoSizeColumn(0);
        myExcelSheet.autoSizeColumn(2);
        myExcelSheet.autoSizeColumn(3);
        myExcelSheet.autoSizeColumn(4);
        myExcelSheet.autoSizeColumn(5);
        myExcelSheet.autoSizeColumn(6);
        myExcelSheet.autoSizeColumn(7);
        myExcelSheet.autoSizeColumn(9);
    }
}
