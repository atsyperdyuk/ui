package excel;

import java.util.ArrayList;
import java.util.List;

import static excel.InitWorkBook.myExcelSheet;
import static excel.InitWorkBook.row;
import static excel.ReadFromExcel.position;

/**
 * Created by Andriy on 9/11/2017.
 */
public class CreatingExpBars {
    static List<ExpenseBar> expenseBarsList = new ArrayList<ExpenseBar>();
    static final int MOSAIC = 0;
    static final int MOSAIC_TYPE = 1;
    static String mosaicName = "";
    static String mosaicType = "";

    public static void getExpenseBars(){
        for (int i = position + 1; i < 200 + position; i++) {
            row = myExcelSheet.getRow(i);
            if (row != null && row.getCell(MOSAIC) != null) {
                mosaicName = row.getCell(MOSAIC).toString();
                if (mosaicName.length() > 0) {

                    //if mosaic type is missed
                    if (row.getCell(MOSAIC_TYPE) == null || row.getCell(MOSAIC_TYPE).toString().length() == 0) {
                        MessageBox.display("The mosaic type is missed, see row " + ++i);
                    }
                    mosaicType = row.getCell(MOSAIC_TYPE).toString();
                    expenseBarsList.add(new ExpenseBar(mosaicName, mosaicType, i));
                    Data.countExpBars++;
                }
            }
        }
    }

    public static void diaplay(){
        for (ExpenseBar expBar : expenseBarsList){
            System.out.println(expBar.getMosaicName());
        }
    }
}
