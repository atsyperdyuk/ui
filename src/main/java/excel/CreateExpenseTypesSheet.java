package excel;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

import static excel.CreatingExpBars.expenseBarsList;
import static excel.InitWorkBook.font;
import static excel.InitWorkBook.*;

/**
 * Created by Andriy on 9/19/2017.
 */
public class CreateExpenseTypesSheet {

    public static void add() {
        int position = 0;
        myExcelSheet = myExcelBook.createSheet("Expenses");
        row = myExcelSheet.createRow(position);
        style = myExcelSheet.getWorkbook().createCellStyle();
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        font = myExcelBook.createFont();
        font.setBold(true);
        style.setFont(font);

        //update for the table tbl_ExpenseReportItemType
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_ExpenseReportItemType");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ExpenseReportItemTypeID");
        row.createCell(2).setCellValue("CustomerID");
        row.createCell(3).setCellValue("Name");
        row.createCell(4).setCellValue("Icon");
        row.createCell(5).setCellValue("Type");
        row.createCell(6).setCellValue("CostCodeID");
        row.createCell(7).setCellValue("FormHeightOffset");
        row.createCell(8).setCellValue("ExpenseBarID");
        row.createCell(9).setCellValue("IsPerDiem");
        row.createCell(10).setCellValue("FlexID");
        row.createCell(11).setCellValue("MobileCategory");
        row.createCell(12).setCellValue("DefaultMatterID");
        row.createCell(13).setCellValue("EnableDefaultMatter");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);
        row.getCell(4).setCellStyle(style);
        row.getCell(5).setCellStyle(style);
        row.getCell(6).setCellStyle(style);
        row.getCell(7).setCellStyle(style);
        row.getCell(8).setCellStyle(style);
        row.getCell(9).setCellStyle(style);
        row.getCell(10).setCellStyle(style);
        row.getCell(11).setCellStyle(style);
        row.getCell(12).setCellStyle(style);
        row.getCell(13).setCellStyle(style);
        int i = 1;
        for (ExpenseBar expBar : expenseBarsList) {
            for (Button button : expBar.getButtons()) {
                for (ExpenseType expType : button.getExpenseTypes()) {
                    row = myExcelSheet.createRow(++position);
                    row.createCell(0).setCellValue("insert");
                    row.createCell(1).setCellValue("~e" + i);
                    row.createCell(2).setCellValue("~Customer");
                    row.createCell(3).setCellValue(expType.getName());
                    row.createCell(4).setCellValue(expType.getIcon());
                    row.createCell(5).setCellValue("LineItem");
                    row.createCell(6).setCellValue("~CostCode");
                    row.createCell(7).setCellValue("0");
                    row.createCell(9).setCellValue("0");
                    row.createCell(11).setCellValue("MOBL");
                    row.createCell(13).setCellValue("0");
                    if (button.getExpenseTypes().size() == 1) {
                        for (ExpenseBar mosaic : expenseBarsList) {
                            if (mosaic.getMosaicName().toLowerCase().matches(expType.getName().toLowerCase())) {
                                row.createCell(8).setCellValue("~ExpBar" + mosaic.getMosaicName());
                            }
                        }
                    }
                    i++;
                }
            }
        }
        row = myExcelSheet.createRow(++position);
        row = myExcelSheet.createRow(++position);

        //update for the table tbl_ScreenObject
        row = myExcelSheet.createRow(++position);
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_ScreenObject");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ScreenObjectID");
        row.createCell(2).setCellValue("CustomerID");
        row.createCell(3).setCellValue("FlexID");
        row.createCell(4).setCellValue("MobileFlag");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);
        row.getCell(4).setCellStyle(style);
        i = 1;
        for (ExpenseBar expBar : expenseBarsList) {
            for (Button button : expBar.getButtons()) {
                for (ExpenseType expType : button.getExpenseTypes()) {
                    row = myExcelSheet.createRow(++position);
                    row.createCell(0).setCellValue("insert");
                    row.createCell(1).setCellValue("~eso" + i);
                    row.createCell(2).setCellValue("~Customer");
                    row.createCell(3).setCellValue("~concat('lbl_tbl_ExpenseReportItemType_',~e" + i + ")");
                    row.createCell(4).setCellValue("0");
                    i++;
                }
            }
        }
        row = myExcelSheet.createRow(++position);
        row = myExcelSheet.createRow(++position);

        //update for the table tbl_ScreenObjectDescription
        row = myExcelSheet.createRow(++position);
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_ScreenObjectDescription");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ScreenObjectID");
        row.createCell(2).setCellValue("LanguageID");
        row.createCell(3).setCellValue("TheValue");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);

        i = 1;
        for (ExpenseBar expBar : expenseBarsList) {
            for (Button button : expBar.getButtons()) {
                for (ExpenseType expType : button.getExpenseTypes()) {
                    row = myExcelSheet.createRow(++position);
                    row.createCell(0).setCellValue("insert");
                    row.createCell(1).setCellValue("~eso" + i);
                    row.createCell(2).setCellValue("1");
                    row.createCell(3).setCellValue(expType.getName());
                    i++;
                }
            }
        }
        row = myExcelSheet.createRow(++position);
        row = myExcelSheet.createRow(++position);
        row = myExcelSheet.createRow(++position);

        //update for the table tbl_ExpenseReportItemType
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_ExpenseReportItemType");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ExpenseReportItemTypeID");
        row.createCell(2).setCellValue("CustomerID");
        row.createCell(3).setCellValue("Name");
        row.createCell(4).setCellValue("Icon");
        row.createCell(5).setCellValue("Type");
        row.createCell(6).setCellValue("CostCodeID");
        row.createCell(7).setCellValue("FormHeightOffset");
        row.createCell(8).setCellValue("ExpenseBarID");
        row.createCell(9).setCellValue("IsPerDiem");
        row.createCell(10).setCellValue("FlexID");
        row.createCell(11).setCellValue("MobileCategory");
        row.createCell(12).setCellValue("DefaultMatterID");
        row.createCell(13).setCellValue("EnableDefaultMatter");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);
        row.getCell(4).setCellStyle(style);
        row.getCell(5).setCellStyle(style);
        row.getCell(6).setCellStyle(style);
        row.getCell(7).setCellStyle(style);
        row.getCell(8).setCellStyle(style);
        row.getCell(9).setCellStyle(style);
        row.getCell(10).setCellStyle(style);
        row.getCell(11).setCellStyle(style);
        row.getCell(12).setCellStyle(style);
        row.getCell(13).setCellStyle(style);
        i = 1;
        for (ExpenseBar expBar : expenseBarsList) {
            for (Button button : expBar.getButtons()) {
                for (ExpenseType expType : button.getExpenseTypes()) {
                    row = myExcelSheet.createRow(++position);
                    row.createCell(0).setCellValue("update");
                    row.createCell(1).setCellValue("~e" + i);
                    row.createCell(2).setCellValue("~Customer");
                    row.createCell(3).setCellValue(expType.getName());
                    row.createCell(4).setCellValue(expType.getIcon());
                    row.createCell(5).setCellValue("LineItem");
                    row.createCell(6).setCellValue("~CostCode");
                    row.createCell(7).setCellValue("0");
                    row.createCell(9).setCellValue("0");
                    row.createCell(10).setCellValue("~concat('lbl_tbl_ExpenseReportItemType_',~e" + i + ")");
                    row.createCell(11).setCellValue("MOBL");
                    row.createCell(13).setCellValue("0");
                    if (button.getExpenseTypes().size() == 1) {
                        for (ExpenseBar mosaic : expenseBarsList) {
                            if (mosaic.getMosaicName().toLowerCase().matches(expType.getName().toLowerCase())) {
                                row.createCell(8).setCellValue("~ExpBar" + mosaic.getMosaicName());
                            }
                        }
                    }
                    i++;
                }
            }
        }

        myExcelSheet.autoSizeColumn(2);
        myExcelSheet.autoSizeColumn(3);
        myExcelSheet.autoSizeColumn(4);
        myExcelSheet.autoSizeColumn(6);
        myExcelSheet.autoSizeColumn(8);

    }


}

