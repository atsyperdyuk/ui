package excel;

import static excel.CreatingExpBars.expenseBarsList;
import static excel.InitWorkBook.myExcelSheet;
import static excel.InitWorkBook.row;
import static excel.ReadFromExcel.position;

/**
 * Created by Andriy on 9/11/2017.
 */
public class CreatingButtons {

    static final int BUTTON = 2;
    static final int BUTTON_ICON = 3;
    static String buttonName = "";
    static String buttonIcon = "";

    public static void getButtons(){
        expenseBarsList.add(new ExpenseBar("End", "",  500));//hard fix
        for (int i = 0; i < expenseBarsList.size() - 1; i++) {
            position = expenseBarsList.get(i).getStartPosition();
            while (position < expenseBarsList.get(i + 1).getStartPosition()) {
                row = myExcelSheet.getRow(position);
                if (row != null && row.getCell(BUTTON) != null) {
                    buttonName = row.getCell(BUTTON).toString();
                    if (buttonName.length() > 0) {
                        //if button icon is missed
                        if (row.getCell(BUTTON_ICON) == null || row.getCell(BUTTON_ICON).toString().length() == 0) {
                            MessageBox.display("The icon is missed, see row " + ++position);
                        }
                        buttonIcon = row.getCell(BUTTON_ICON).toString();
                        expenseBarsList.get(i).addButton(new Button(buttonName, buttonIcon, position));
                        Data.countButtons++;
                    }
                }
                position++;
            }
        }
        expenseBarsList.remove(expenseBarsList.size() - 1);//removing hard fix
    }

    public static void display(){
        for (ExpenseBar expBar : expenseBarsList){
            System.out.println(expBar.getMosaicName());
            for (Button button : expBar.getButtons()){
                System.out.println("    " + button.getName());
            }
        }
    }
}
