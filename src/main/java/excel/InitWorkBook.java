package excel;

import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileInputStream;

/**
 * Created by Andriy on 9/1/2017.
 */
public class InitWorkBook {
    public static Workbook myExcelBook = null;
    public static Sheet myExcelSheet = null;
    public static Row row = null;
    public static CellStyle style = null;
    public static Font font = null;

    public static void getWorkBook(String file) {
        try {
            if (file.endsWith(".s")) {
                // if extension of file is .xls
                myExcelBook = new HSSFWorkbook(new FileInputStream(file));
            } else {
                // if extension of file is .xlsx
                myExcelBook = new XSSFWorkbook(new FileInputStream(file));
            }

            //if there is no 'Mosaics' tab in the Workbook
            if(myExcelBook.getSheet("Mosaics") == null) {MessageBox.display("Can't find 'Mosaics' sheet in the Workbook");}
            myExcelSheet = myExcelBook.getSheet("Mosaics");
            row = myExcelSheet.getRow(0);
        } catch (Exception e) {
            MessageBox.display("Can't find the file");
        }

    }
}
