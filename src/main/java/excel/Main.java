package excel;

import javax.swing.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Andriy on 8/27/2017.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        //String file = "build.xlsx";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-HH-mm"); //Or whatever format fits best your needs.
        String dateStr = sdf.format(date);

        JFileChooser fileopen = new JFileChooser();
        int ret = fileopen.showDialog(null, "Choose the file");
        if (ret != 1 && ret == JFileChooser.APPROVE_OPTION) {
            String file = fileopen.getSelectedFile().toString();
            ReadFromExcel.readFromExcel(file);
            WriteIntoExcel.writeIntoExcel("build-" + dateStr + ".xls");
            MessageBox.display("Done");
        }

    }

}
