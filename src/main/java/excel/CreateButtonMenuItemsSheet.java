package excel;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

import static excel.CreatingExpBars.expenseBarsList;
import static excel.InitWorkBook.*;
import static excel.InitWorkBook.row;

/**
 * Created by Andriy on 9/21/2017.
 */
public class CreateButtonMenuItemsSheet {

    public static void add() {
        int i = 1;
        int b = 1;
        int e = 1;
        int position = 0;
        myExcelSheet = myExcelBook.createSheet("ButtonMenuItems");
        row = myExcelSheet.createRow(0);
        style = myExcelSheet.getWorkbook().createCellStyle();
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        font = myExcelBook.createFont();
        font.setBold(true);
        style.setFont(font);

        //update for the table tbl_ButtonMenuItem
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_ButtonMenuItem");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ButtonMenuItemID");
        row.createCell(2).setCellValue("ButtonID");
        row.createCell(3).setCellValue("CustomerID");
        row.createCell(4).setCellValue("Label");
        row.createCell(5).setCellValue("SortOrder");
        row.createCell(6).setCellValue("ExpenseReportItemTypeID");
        row.createCell(7).setCellValue("FlexID");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);
        row.getCell(4).setCellStyle(style);
        row.getCell(5).setCellStyle(style);
        row.getCell(6).setCellStyle(style);
        row.getCell(7).setCellStyle(style);
        for (ExpenseBar expBar : expenseBarsList) {
            for (Button button : expBar.getButtons()) {
                int sortOrder = 10;
                for (ExpenseType expType : button.getExpenseTypes()) {
                    if (button.getExpenseTypes().size() > 1) {
                        row = myExcelSheet.createRow(++position);
                        row.createCell(0).setCellValue("insert");
                        row.createCell(1).setCellValue("~bm" + i);
                        row.createCell(2).setCellValue("~b" + b);
                        row.createCell(3).setCellValue("~Customer");
                        row.createCell(4).setCellValue(expType.getName());
                        row.createCell(5).setCellValue(sortOrder);
                        row.createCell(6).setCellValue("~e" + e);
                        sortOrder += 10;
                        i++;
                    }
                    e++;
                }
                b++;
            }
        }
        row = myExcelSheet.createRow(++position);
        row = myExcelSheet.createRow(++position);
        row = myExcelSheet.createRow(++position);

        //update for the table tbl_ScreenObject
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_ScreenObject");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ScreenObjectID");
        row.createCell(2).setCellValue("CustomerID");
        row.createCell(3).setCellValue("FlexID");
        row.createCell(4).setCellValue("MobileFlag");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);
        row.getCell(4).setCellStyle(style);
        i = 1;
        for (ExpenseBar expBar : expenseBarsList) {
            for (Button button : expBar.getButtons()) {
                for (ExpenseType expType : button.getExpenseTypes()) {
                    if (button.getExpenseTypes().size() > 1) {
                        row = myExcelSheet.createRow(++position);
                        row.createCell(0).setCellValue("insert");
                        row.createCell(1).setCellValue("~bmso" + i);
                        row.createCell(2).setCellValue("~Customer");
                        row.createCell(3).setCellValue("~concat('lbl_tbl_ButtonMenuItem_',~bm" + i +")");
                        row.createCell(4).setCellValue(0);
                        i++;
                    }
                }
            }
        }
        row = myExcelSheet.createRow(++position);
        row = myExcelSheet.createRow(++position);
        row = myExcelSheet.createRow(++position);

        //update for the table tbl_ScreenObjectDescription
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_ScreenObjectDescription");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ScreenObjectID");
        row.createCell(2).setCellValue("LanguageID");
        row.createCell(3).setCellValue("TheValue");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);
        i = 1;
        for (ExpenseBar expBar : expenseBarsList) {
            for (Button button : expBar.getButtons()) {
                for (ExpenseType expType : button.getExpenseTypes()) {
                    if (button.getExpenseTypes().size() > 1) {
                        row = myExcelSheet.createRow(++position);
                        row.createCell(0).setCellValue("insert");
                        row.createCell(1).setCellValue("~bmso" + i);
                        row.createCell(2).setCellValue("1");
                        row.createCell(3).setCellValue(expType.getName());
                        i++;
                    }
                }
            }
        }
        row = myExcelSheet.createRow(++position);
        row = myExcelSheet.createRow(++position);
        row = myExcelSheet.createRow(++position);

        //update for the table tbl_ButtonMenuItem
        row.createCell(0).setCellValue("Table");
        row.createCell(1).setCellValue("tbl_ButtonMenuItem");
        row = myExcelSheet.createRow(++position);
        row.createCell(1).setCellValue("ButtonMenuItemID");
        row.createCell(2).setCellValue("ButtonID");
        row.createCell(3).setCellValue("CustomerID");
        row.createCell(4).setCellValue("Label");
        row.createCell(5).setCellValue("SortOrder");
        row.createCell(6).setCellValue("ExpenseReportItemTypeID");
        row.createCell(7).setCellValue("FlexID");
        row.getCell(1).setCellStyle(style);
        row.getCell(2).setCellStyle(style);
        row.getCell(3).setCellStyle(style);
        row.getCell(4).setCellStyle(style);
        row.getCell(5).setCellStyle(style);
        row.getCell(6).setCellStyle(style);
        row.getCell(7).setCellStyle(style);
        i = 1;
        b = 1;
        e = 1;
        for (ExpenseBar expBar : expenseBarsList) {
            for (Button button : expBar.getButtons()) {
                int sortOrder = 10;
                for (ExpenseType expType : button.getExpenseTypes()) {
                    if (button.getExpenseTypes().size() > 1) {
                        row = myExcelSheet.createRow(++position);
                        row.createCell(0).setCellValue("update");
                        row.createCell(1).setCellValue("~bm" + i);
                        row.createCell(2).setCellValue("~b" + b);
                        row.createCell(3).setCellValue("~Customer");
                        row.createCell(4).setCellValue(expType.getName());
                        row.createCell(5).setCellValue(sortOrder);
                        row.createCell(6).setCellValue("~e" + e);
                        row.createCell(7).setCellValue("~concat('lbl_tbl_ButtonMenuItem_',~bm" + i + ")");
                        sortOrder += 10;
                        i++;
                    }
                    e++;
                }
                b++;
            }
        }

        myExcelSheet.autoSizeColumn(1);
        myExcelSheet.autoSizeColumn(2);
        myExcelSheet.autoSizeColumn(3);
        myExcelSheet.autoSizeColumn(4);
        myExcelSheet.autoSizeColumn(5);
        myExcelSheet.autoSizeColumn(6);
    }
}
